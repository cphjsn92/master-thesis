\chapter{Fundamentals}

\section{Lie group and Lie algebra}
A group is a set $\circ: G\times G\rightarrow G$ satisfying four properties,

\begin{enumerate}
	\item \textbf{Closure}, $\forall a,b \in G,\: a \circ b \in G$  
	\item \textbf{Associativity}, $\forall a,b,c \in G,\: (a \circ b) \circ c = a \circ ( b \circ c)$  
	\item \textbf{Identity element}, $\exists e \in G,\; s.t \; \forall x \in G, \: e \circ x = x \circ e = x$ 
	\item \textbf{Inverse element}, $\forall a \in G, \exists b \in G \; s.t \; e \circ x = x \circ e = x$ 
\end{enumerate}
Lie group is a group with continuous elements. A rigid body motion is composed of translation and rotation. These are continuous motion in space, so that they can be represented by lie group. Lie algebra is the generator of Lie group, which lives in the tangent space of Lie group\cite{ethan14lie}\cite{lierobotic18}. The adjoint of Lie algebra is used to transform the Lie algebra from one tangent space to another. To define the adjoint of Lie algebra, suppose that G is a Lie Group and $\mathcal{L}(G)$ is the Lie algebra associated with G, for $R \in G$ and $\Phi \in \mathcal{L}(G)$, the adjoint of Lie algebra is showed as follows,

\begin{equation}
\mathrm{ad}_R \Phi = \ln(R \Phi^\wedge R^{-1})^\vee
\end{equation}

where $^\wedge$ changes the Lie algebra to the correspondent element in Lie Group and $\vee$ changes the element from Lie Group to its correspondent element in Lie Algebra. \\
\\
\noindent
From the above definition, the following equation hold for any $\Phi \in \mathcal{R}^d$, where d is the degree of freedom of the Lie group.
\begin{equation}
R e^\Phi= e^{\mathrm{ad}_R \Phi} R
\label{eqn:adj_def_2}
\end{equation}

\noindent
To perform efficient optimization with Gaussian-Newton or Levenberg-Marquardt algorithms, we need to define Jacobians that represent a linearization of the cost function. Since the addition of two elements from Lie group is not in Lie group, the traditional definition of differentiation ($\frac{f(x+\Delta x) - f(x)}{\Delta x}$) is not suitable for Lie group. Here, we will use left-tangent space perturbation to define differentiation. We just need to be careful when we update the minimizer after each iteration of optimization.

\subsection{Rotation}
The special orthogonal group, representing rotation,
\begin{equation}
SO(3) = \lbrace R \in \mathbb{R}^{3x3}| R^T R = R R^T = \textbf{I}, \mathrm{det} R  = 1 \rbrace
\end{equation}
SO(3) is a non-commutive group, which means that $R_1,R_2 \in SO(3) \nRightarrow R_1 R_2 = R_2 R_1$. We can use exponential map to change Lie group to the corresponding Lie algebra and vice versa.
\begin{equation}
R = e^{\Phi}
\end{equation}
where $R \in SO(3)$ and $\Phi \in so(3)$.\\
\\
$\Phi$ is an anti-symmetric matrix, we can write it as a 3-vector $\phi$. People use $\wedge$ and $\vee$ to interchange these two forms of Lie algebra (only $\Phi = \phi^\wedge$ satisfies the definition of Lie algebra).
\begin{equation}
\Phi = \phi^\wedge =
\begin{pmatrix}
0       && -\phi_3 && \phi_2 \\
 \phi_3 && 0       && -\phi_1 \\
-\phi_2 && \phi_1  && 0
\end{pmatrix}
\Longleftrightarrow
\phi = \Phi^\vee =
\begin{pmatrix}
-\Phi_{23}   \\ \Phi_{13} \\ -\Phi_{12}
\end{pmatrix}
\end{equation}
By expanding the exponential, we get a close-form transformation from $\phi$ to R so called Rodrigues formula. Let $a=\frac{\phi}{|\phi|}$, we obtain
\begin{equation}
R = \cos\phi + (1-\cos\phi)aa^T + \sin\phi \, a^\wedge
\end{equation}
and its inverse
\begin{equation}
\ln(R)=\frac{\theta}{2 \sin\theta}(R-R^T)
\label{eqn:InR}
\end{equation}
\begin{equation}
\theta=\arccos(\frac{\mathrm{tr}(R)-1}{2})
\end{equation}

\subsubsection{Adjoint}
Adjoint of Lie algebra will be used when we calculate the derivative of relative rotation with respect to an absolute rotation. For $\Phi \in so(3)$ and $R \in SO(3)$, let $\Phi = w t$, we take derivative of eq.\ref{eqn:adj_def_2} with respect to t and perform taylor expansion, we obtain
\begin{equation}
\mathrm{ad}_R = R
\end{equation}

\subsubsection{Jacobians}

Let $p_0$ to be the original point, $p=Rp_0$ is the transformed point ,then
\begin{equation}
\frac{\partial p}{\partial R} \equiv \frac{\partial p}{\partial \phi_R} = \frac{\partial}{\partial w}e^{w^\wedge} R p_0 |_{w=0} = -p^{\wedge}
\end{equation}
where $\phi_R = \ln(R)$ and $w \in \mathbb{R}^3$.\\
\\
REMARKS: When there is a derivative w.r.t a Lie algebra, we will change the symbol of Lie algebra to its Lie group for convenience, for exmaple, $\frac{\partial p}{\partial \phi_R} \equiv \frac{\partial p}{\partial R}$. \\
\\
To calculate the jacobian of relative rotation, let $R_2 = R_1^T R_0$, $R_3 = R_1 R_0^T$ and again using left tangent space perturbation to define differentiation ($e^\epsilon R_2 = R_1 e^{w^\wedge} R_0$) and $R_1 e^{w^\wedge}= e^{\mathrm{ad}_{R_1} w} R_1$
\begin{equation}
\frac{\partial R_2}{\partial R_0} = \frac{\partial \ln(R_1^T e^{w^\wedge} R_0 R_2^{-1})}{\partial w} |_{w=0} = \mathrm{ad}_{R_1^T} = R_1^T
\end{equation}
\begin{equation}
\frac{\partial R_2}{\partial R_1} = \frac{\partial \ln(R_1^T e^{-w^\wedge} R_0 R_2^{-1})}{\partial w} |_{w=0} = -\mathrm{ad}_{R_1^T} = -R_1^T
\end{equation}
\begin{equation}
\frac{\partial R_3}{\partial R_0} = \frac{\partial \ln(R_3 e^{-w^\wedge} R_3^{T})}{\partial w} |_{w=0} = -\mathrm{ad}_{R_3} = -R_3
\end{equation}
\begin{equation}
\frac{\partial R_3}{\partial R_1} = \frac{\partial \ln(e^{w^\wedge} R_1 R_0^T R_3^{-1})}{\partial w} |_{w=0} = I_{3x3}
\end{equation}

\subsection{Rigid body transformation in 2D}
The special Euclidean group represents the pose in 2-dimension. This will be used in optical flow algorithm.
\begin{equation}
\mathrm{SE}(2) = \lbrace S=
\begin{bmatrix}
R   && t\\
0^T && 1   
\end{bmatrix}, \in \mathbb{R}^{3\times3}| R\in SO(2), t \in \mathbb{R}^{2} \rbrace
\end{equation}

\noindent
SE(2) is a non-coummutive group. Each element is composed of 2d rotation and 2d translation. Again, we can use the exponential map to transform a Lie algebra element to its corresponding Lie group element and vice versa. In order to do so, similar to the SO(3) case, we first find the generators of the group by
\begin{equation}
G_i=\frac{\partial}{\partial \alpha_i} S
\end{equation}
where $\alpha_i \in \lbrace u_x, u_y, \theta \rbrace$​, \textbf{u} is the translation vector and the $\theta$ is the rotation angle.
\begin{multicols}{3}
  \begin{equation}
    G_1 = 
    \begin{pmatrix}
      0 && 0 && 1\\
      0 && 0 && 0\\
      0 && 0 && 0  
    \end{pmatrix}
  \end{equation}\break
   \begin{equation}
    G_2 = 
    \begin{pmatrix}
      0 && 0 && 0\\
      0 && 0 && 1\\
      0 && 0 && 0  
    \end{pmatrix}
  \end{equation}\break
  \begin{equation}
    G_3 = 
    \begin{pmatrix}
      0 && -1 && 0\\
      1 &&  0 && 0\\
      0 &&  0 && 0  
    \end{pmatrix}
  \end{equation}
\end{multicols}

\noindent
So, we define $\delta = \begin{pmatrix} u_x &&  u_y && \theta \end{pmatrix}^T$, such that
\begin{equation}
\Delta = \delta^\wedge = \alpha_i G_i = 
\begin{pmatrix}
  \theta \sigma_x && \textbf{u} \\
  0               &&  0 
\end{pmatrix}
\end{equation}
where $\sigma_x = 
\begin{pmatrix}
  0 && -1 \\
  1 &&  0 
\end{pmatrix}$.\\
\\
\noindent
By expanding the exponential, we get a close-form transformation from $\delta$ to S,
\begin{equation}
S = 
\begin{pmatrix}
  e^{\theta \sigma_x} && V\textbf{u} \\
  0 &&  1 
\end{pmatrix}
\end{equation}
where $V = \frac{1}{\theta} 
\begin{pmatrix}
  \sin\theta   && -(1-\cos\theta) \\
  1-\cos\theta && \sin\theta
\end{pmatrix}$
and its inverse
\begin{equation}
\ln(S)=
\ln\begin{pmatrix}
  R && t \\
  0 && 1
\end{pmatrix}
=\begin{pmatrix}
  V^{-1} t  \\
  \theta
\end{pmatrix}
\end{equation}
\begin{equation}
\theta = \arctan(\frac{R_{21}}{R_{11}})
\end{equation}

\subsubsection{Adjoint}
Adjoint of se(2) is defined as follows,
\begin{equation}
S e^\Delta= e^{\mathrm{ad}_S \Delta} S
\label{eqn:def_se2_adj}
\end{equation}
Let $\Delta = w t$, take derivative with respect to t and perform taylor expansion, we can get,
\begin{equation}
\mathrm{ad}_S = \begin{pmatrix}
  R && -\sigma_x \textbf{t} \\
  0 && 1
\end{pmatrix}
\end{equation}

\subsubsection{Jacobians}
Jacobians will be used to minimize the photometric error later in optical flow. Let $p_0$ to be the origin point and $p=Sp_0$ the transformed point, then
\begin{equation}
\frac{\partial p}{\partial S} = \frac{\partial}{\partial w}e^{w^\wedge} S p_0 |_{w=0} = \begin{pmatrix}
  \mathbb{I}_{2 \times 2} && \sigma_x \textbf{t} 
\end{pmatrix}
\label{eqn:se2_point_jacobian}
\end{equation}

\subsection{Ridge body transformation in 3D}
The special Euclidean group represents the pose in 3-dimension. This will be used in bundle adjustment.
\begin{equation}
SE(3) = \lbrace T=
\begin{bmatrix}
R   && t\\
0^T && 1   
\end{bmatrix}, \in \mathbb{R}^{4 \times 4}| R\in SO(3), t \in \mathbb{R}^{3} \rbrace
\end{equation}

\noindent
SE(3) is also a non-coummutive group. Each element is composed of 3d rotation and 3d translation. Once again, we can use the exponential map to change Lie group to the corresponding Lie algebra and vice versa. In order to do so, just like the SO(3) and SE(2) case, we first find the generators of the group by
\begin{equation}
G_i=\frac{\partial}{\partial \alpha_i} S
\end{equation}
where $\alpha_i \in \lbrace u_x, u_y, u_z, w_1, w_2, w_3 \rbrace$.
\begin{multicols}{3}
  \begin{equation}
    G_1 = 
    \begin{pmatrix}
      0 && 0 && 0 && 1\\
      0 && 0 && 0 && 0\\
      0 && 0 && 0 && 0\\ 
      0 && 0 && 0 && 0
    \end{pmatrix}
  \end{equation}\break
   \begin{equation}
    G_2 = 
    \begin{pmatrix}
      0 && 0 && 0 && 0\\
      0 && 0 && 0 && 1\\
      0 && 0 && 0 && 0\\ 
      0 && 0 && 0 && 0
    \end{pmatrix}
  \end{equation}\break
  \begin{equation}
    G_3 = 
    \begin{pmatrix}
      0 && 0 && 0 && 0\\
      0 && 0 && 0 && 0\\
      0 && 0 && 0 && 1\\ 
      0 && 0 && 0 && 0
    \end{pmatrix}
  \end{equation}
\end{multicols}
\begin{multicols}{3}
  \begin{equation}
    G_4 = 
    \begin{pmatrix}
      0 && 0 && 0 && 0\\
      0 && 0 && -1 && 0\\
      0 && 1 && 0 && 0\\ 
      0 && 0 && 0 && 0
    \end{pmatrix}
  \end{equation}\break
   \begin{equation}
    G_5 = 
    \begin{pmatrix}
      0 && 0 && 1 && 0\\
      0 && 0 && 0 && 0\\
      -1 && 0 && 0 && 0\\ 
      0 && 0 && 0 && 0
    \end{pmatrix}
  \end{equation}\break
  \begin{equation}
    G_6 = 
    \begin{pmatrix}
      0 && -1 && 0 && 0\\
      1 && 0 && 0 && 0\\
      0 && 0 && 0 && 0\\ 
      0 && 0 && 0 && 0
    \end{pmatrix}
  \end{equation}
\end{multicols}

\noindent
So, we define $\delta = \left[  u_x, u_y, u_z, w_1, w_2, w_3 \right]^T = \left[ \textbf{u}, \textbf{w} \right]^T$, such that
\begin{equation}
\Delta = \delta^\wedge = \alpha_i G_i = 
\begin{pmatrix}
  \textbf{​w}^\wedge && \textbf{u} \\
  0               &&  0 
\end{pmatrix}
\end{equation}

\noindent
By expanding the exponential, we get a close-form transformation from $\delta$ to S.
\begin{equation}
T = 
\begin{pmatrix}
  R && V\textbf{u} \\
  0 &&  1 
\end{pmatrix}
\end{equation}
where $\theta = |w|$, $V = \textbf{I} + \frac{\sin\theta}{\theta} w^\wedge + \frac{1-\cos\theta}{\theta^2} (w^{\wedge})^2$ is the Rodrigues formula and $V =  \textbf{I} +\frac{1-\cos\theta}{\theta^2} w^\wedge + \frac{1-\frac{\sin\theta}{\theta}}{\theta^2} (w^{\wedge})^2$
and it inverse just
\begin{equation}
\ln(T)=
\ln\begin{pmatrix}
  R && t \\
  0 && 1
\end{pmatrix}
=\begin{pmatrix}
  V^{-1} t  \\
  \ln R
\end{pmatrix}
\end{equation}
where $\ln R$ can be calculated by eq.\ref{eqn:InR}

\subsubsection{Adjoint}
Adjoint of se(3) is defined as follows,
\begin{equation}
T e^\Delta= e^{\mathrm{ad}_T \Delta} T
\label{eqn:adjoint_se3_1}
\end{equation}
Let $\Delta = w x$, take derivative with respect to x and perform taylor expansion, we can get,
\begin{equation}
\mathrm{ad}_T = \begin{pmatrix}
  R && \textbf{t}^\wedge R \\
  0 && R
\end{pmatrix}
\in \mathbb{R}_{6 \times 6}
\label{eqn:adjoint_se3_2}
\end{equation}

\subsubsection{Jacobians}
Jacobians will be used to optimize the relative poses (for example $T_{wt}^{-1}T_{wh}$) later in the section of optical flow. Let $p_0$ to be the origin point, $p_t=T_{th} p_h$ the transformed point, 
\begin{equation}
\frac{\partial p}{\partial T} = \frac{\partial}{\partial w}e^{w^\wedge} T p_0 |_{w=0} = \begin{pmatrix}
  \textbf{I}_{3 \times 3} && -\textbf{p}^\wedge 
\end{pmatrix}
\label{eqn:jacobian_se3_point_1}
\end{equation}
For calculating the jacobian of relative pose, let $T_{th} = T_{wt}^{-1} T_{wh}$ and $T'_{th} = T_{tw} T_{hw}^{-1}$, note that even $T_{th} = T'_{th}$, their jacobians are not the same due to the fact that we use left tangent space perturbation to define derivative of poses, and again use the left tangent space perturbation to define differentiation, $T e^{w^\wedge}= e^{\mathrm{ad}_{T} w^\wedge} T$ and $T_{wt}^{-1} T_{wh} T_{th}^{-1} = \textbf{I}_{4x4}$
\begin{equation}
\frac{\partial T_{th}}{\partial T_{wh}} = \frac{\partial \ln(T_{wt}^{-1}  e^{w^\wedge} T_{wh} T_{th}^{-1})}{\partial w} |_{w=0} = \mathrm{ad}_{T_{wt}^{-1}}
\label{eqn:jacobian_se3_pose_1}
\end{equation}
\begin{equation}
\frac{\partial T_{th}}{\partial T_{wt}} = \frac{\partial \ln(T_{wt}^{-1}  e^{-w^\wedge} T_{wh} T_{th}^{-1})}{\partial w} |_{w=0} = -\mathrm{ad}_{T_{wt}^{-1}}
\label{eqn:jacobian_se3_pose_2}
\end{equation}
\begin{equation}
\frac{\partial T_{th}'}{\partial T_{wh}} = \frac{\partial \ln(T_{tw} T_{hw}^{-1} e^{-w^\wedge} T_{th}'^{-1})}{\partial w} |_{w=0} = -\mathrm{ad}_{T_{th}}
\end{equation}
\begin{equation}
\frac{\partial T_{th}'}{\partial T_{wt}} = \frac{\partial \ln(e^{w^\wedge} T_{tw} T_{hw}^{-1}  T_{th}'^{-1})}{\partial w} |_{w=0} = \textbf{I}_{6 \times 6}
\end{equation}

\section{Inertial measurement unit (IMU)}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=7cm]{Figure/ch2/IMU_coord}
  \caption{IMU coordinate}
\end{figure}

\noindent
Inertial measurement unit, also known as IMU is composed of a gyroscope and accelerometer. Gyroscope is used to measure the angular velocity with respect to its own IMU coordinate. Accelerometer is then used to measure the linear acceleration.\\

\textbf{A gyroscope} measurement can be split into three parts, the ground true angular velocity (w), bias ($b_g$) and zero Gaussian noise ($\eta_g$). 
\begin{equation}
\tilde{w} = w + b_g + \eta_g
\label{eqn:gyro_model}
\end{equation}
where $\eta_g = N(0,\sigma^2_{gyro})$.\\
\\
\indent
\textbf{An accelerometer} measurement can be split into four parts, the acceleration of IMU in world coordinate ($\tensor[^w]{a}{}$),  the gravity acceleration in world coordinate ($\tensor[^w]{g}{}$), bias ($\tensor[^b]{b}{_a}$) and (3) zero Gaussian noise ($\tensor[^b]{\eta}{_a}$). 
\begin{equation}
\tensor[^b]{\tilde{a}}{} = R_{bw} (\tensor[^w]{a}{} - \tensor[^w]{g}{}) + \tensor[^b]{b}{_a}  + \tensor[^b]{\eta}{_a} 
\label{eqn:accel_model}
\end{equation}
where $^b$ and $^w$ denote quantities in IMU coordinate and world coordinate respectively, and $\eta_a = N(0,\sigma^2_{acc})$.\\
\\
To transform the measurements of IMU into pose and velocity, we use the kinematic equations,
\begin{equation}
R_{wb}(t + \Delta t) = R_{wb}(t) \; e^{(w(t) \Delta t)^\wedge}
\end{equation}

\begin{equation}
\tensor[^w]{v(t + \Delta t)}{} = \tensor[^w]{v(t)}{} + \tensor[^w]{a(t)}{}  \Delta t
\end{equation}

\begin{equation}
\tensor[^w]{s(t + \Delta t)}{} = \tensor[^w]{s(t)}{} + \tensor[^w]{v(t)}{} \Delta t + \frac{1}{2} \tensor[^w]{a(t)}{} \Delta t^2
\end{equation}

\noindent
To use the measurements for predicting the next state, we insert eq. \ref{eqn:gyro_model} and eq. \ref{eqn:accel_model} into kinematic equations, 

\begin{equation}
R_{wb}(t + \Delta t) = R_{wb}(t) \; exp(((\tilde{w}(t) - b_g(t)- \eta_g(t)) \Delta t)^\wedge)
\end{equation}

\begin{equation}
\tensor[^w]{v(t + \Delta t)}{} = \tensor[^w]{v(t)}{} + \tensor[^w]{g}{} \Delta t + R_{wb}(t) (\tensor[^b]{\tilde{a(t)}}{} - \tensor[^b]{b}{_a}  -  \tensor[^b]{\eta}{_a} ) \Delta t
\end{equation}

\begin{equation}
\tensor[^w]{s(t + \Delta t)}{} = \tensor[^w]{s(t)}{} + \tensor[^w]{v(t)}{} \Delta t + \frac{1}{2} \tensor[^w]{g}{} \Delta t^2 + \frac{1}{2}R_{wb}(t)(\tensor[^b]{\tilde{a(t)}}{} - \tensor[^b]{b}{_a}  -  \tensor[^b]{\eta}{_a}) \Delta t^2
\end{equation}


\section{Camera models and Calibration}
A camera is an electronic device to project a 3D world into a 2D plane, we will introduce several camera models.

\subsection{Coordinate system}
We need different kinds of coordinate in a bid to represent locations of landmarks, poses of cameras and keypoints in images. Camera models are the ways to transform coordinate between these coordinate systems.

\begin{figure}[htb]
  \centering
  \includegraphics[width=7cm]{Figure/ch2/world_camera_pixel_coord}
  \caption{World, camera and pixel coordinate}
\end{figure}

\subsubsection{World coordinate}
The world coordinate can be fixed by given a prior map or using the first frame as reference frame if there is no prior coordinate system. 

\subsubsection{Camera coordinate}
The origin of the camera coordinate is located at camera center and one of the axes is aligned to the principal axis. The transformation between world coordinate and camera coordinate is a rigid body transformation.

\subsubsection{Image coordinate}
The image coordinate is obtained by projecting corresponding camera coordinate onto an image plane.

\subsection{Camera models}
We will represent pixel coordinate as $\textbf{u} = [u,v,1]^T = [\tilde{u},1]^T \in \mathbb{P}^2$ and 3D points in camera coordinate as $\textbf{X} = [X,Y,Z,1]^T = [\tilde{X},1]^T \in \mathbb{P}^3$. Also, camera models are mapping from camera coordinate to pixel coordinate $\pi:\mathbb{P}^3\rightarrow \mathbb{P}^2$, its inverse $\pi^{-1}:\mathbb{P}^2\rightarrow \mathbb{P}^3$ unproject image coordinate to the bearing vector.\\
\\
There are several famous camera models, which are based on different theories and have different numbers of degree of freedom, for example, pinhole camera model (4 DoF), field-of-view camera model (5 DoF), unified camera model(5 DoF), extended unified camera model (6 DoF)\cite{7342909}, double sphere camera model (6 DoF)\cite{usenko18double_sphere} and Kannala-Brandt camera model (6 or 8 DoF)\cite{kbcameramodel}. In this section, we will have a brief introduction to the pinhole camera model and double sphere model, the former is the simplest camera model and the latter will be used in this thesis.

\subsubsection{Pinhole camera model}

\begin{figure}[htb]
  \centering
  \includegraphics[width=7cm]{Figure/ch2/pinhole_cam}
  \caption{Pinhole camera model}
\end{figure}

\noindent
Pinhole camera model is a linear transformation between image coordinate and pixel coordinate, which has four degree of freedom $i = [f_x,f_y,c_x,c_y]$ with the projection function below,
\begin{equation}
\textbf{u} 
= \begin{pmatrix}
u\\
v\\
1
\end{pmatrix}
=\pi(\tilde{X},i) \circ \textbf{X}
=\begin{bmatrix}
f_x & 0    & c_x & 0 \\
0   & f_y  & c_y & 0 \\
0   & 0    & 1   & 0 
\end{bmatrix}
\begin{pmatrix}
X \\
Y \\
Z \\
1 
\end{pmatrix}
\end{equation}


\subsubsection{Double sphere camera model}

\begin{figure}[htb]
  \centering
  \includegraphics[scale=0.4]{Figure/ch2/double_sphere}
  \caption[Double sphere camera model]{Double sphere camera model\cite{usenko19nfr}}
\end{figure}

\noindent
Double sphere camera model is a non-linear transformation between camera coordinate and pixel coordinate. It has a close-form inverse and six degree of freedom $i = [f_x,f_y,c_x,c_y,\xi,\alpha]$, while the extra two degrees of freedom are used to deal with the image distortion created by the camera, for example, fish eye camera. The projection function is defined as,

\begin{equation}
\textbf{u} 
= \begin{pmatrix}
u\\
v\\
1
\end{pmatrix}
=\pi(\tilde{X},i) \circ \textbf{X}
=\begin{pmatrix}
f_x\frac{X}{\alpha d_2 + (1-\alpha)(\xi d_1 + Z)} + c_x \\
f_y\frac{Y}{\alpha d_2 + (1-\alpha)(\xi d_1 + Z)} + c_y \\
1 
\end{pmatrix}
\end{equation}

\begin{equation}
d_1 = \sqrt{X^2+Y^2+Z^2}
\end{equation}

\begin{equation}
d_2 = \sqrt{X^2+Y^2+(\xi d_1 + Z)^2}
\end{equation}


\section{Features detection}
Features are special subset of the image which contains more information, so they are easily described and robust to view of angle, for example, corners, lines and blobs etc. There are two types of feature detection, indirect and direct method.

\subsection{Indirect method}
An image is composed of a matrix of brightness or colours (red, blue and green). One of the common ways to compare two images is to find several representative points in both images, and turn the brightness around those representative points into another mathematical representation which can be compared to each other easily. A good mathematical representation for a feature should fulfill requirements below. 

\begin{figure}%
    \centering
    \subfloat[Geometric change\cite{cvbook_szeliski}]{{\includegraphics[width=7cm]{Figure/ch2/different_warping} }}%
    \qquad
    \subfloat[Photometric change]{{\includegraphics[width=7cm]{Figure/ch2/photometric_robustness} }}%
    \caption{Geometric and photometric changes}%
    \label{fig:invariance_example}%
\end{figure}

\begin{enumerate}
	\item Distinctiveness: Different features should have distinct feature descriptor.
	\item Invariance: The same feature can be found across the images and it should be regardless to the geometric (translation, rotation, affine, projective transformation) and photometric (brightness) changes. See Fig \ref{fig:invariance_example}
    \item Robustness: The feature descriptors are robust to noise, blur, quantization and etc.
	\item Efficiency: The number of features should be much smaller than the number of pixel in the image.
    \item Locality: The features are created by the pixels nearby the feature corners, which make the feature robust to clutter and occlusions.
\end{enumerate}

\noindent
There are many features extraction algorithms developed, such as Scale-invariant feature transform (SIFT)\cite{Lowe03distinctiveimage}, Speeded up robust features (SURF)\cite{Bay_surf}, Oriented Features from Accelerated Segment Test (FAST)\cite{Rosten05fusingpoints} and Binary Robust Independent Elementary Features (BRIEF)\cite{Calonder10brief}. In the following, we will briefly introduce Oriented FAST corner detection and BRIEF descriptor which are used in our thesis. 

\subsubsection{Oriented FAST corner detection}

\begin{figure}[htb]
  \centering
  \includegraphics[width=15cm]{Figure/ch2/FAST_corner}
  \caption[Fast corner detection]{Fast corner detection\cite{Rosten05fusingpoints}}
  \label{fig:fast_corner_detection}%
\end{figure}

The first step to construct features is finding "interesting points" in the image. The FAST corner detector\cite{Rosten05fusingpoints} was originally developed by Edward Rosten and Tom Drummond in 2006. It has been widely used in many computer vision tasks because of its computational efficiency. It is faster than many other well-known feature extraction methods, such as (1) Difference of Gaussians (DoG) which requires to apply gaussian filter for several times to the image and (2) Harris corner detectors which needs to calculate the derivative of x- and y-direction for each pixel in order to calculate Harris score.\\
\\
In the contrary, computing FAST corner score does not require to calculate any derivative but just compare the brightness with its neighbour. The steps are as follows:
 
\begin{enumerate}
	\item Assume its brightness is $I_p \in [0,255]$ for a selected pixel p.
	\item Set a threshold T to distinguish the testing pixel being inside or outside the corner.
    \item Draw a circle centered at pixel p with radius 3 pixels, there are 16 pixels on the circumstances. 
	\item If there are at least N contiguous pixels that are either all T brighter or T darker than $I_p$, than we consider pixel p a feature corner, we usually take N=12.
    \item Apply the above steps to every pixel in the image.
\end{enumerate}

\noindent
To make the algorithm faster, we can apply an high-speed test for rejecting non-corner points. For N=12, we test only the pixel 1, 4, 9, 13. Pixel p can only be a corner if and only if at least 3 out of 4 sampling pixels that are all brighter or darker than $I_p$. Moreover, since the neighbour of a pixel should also have similar FAST corner score to some extent, it would lead to too many feature corners in a small region of the image. In order to tackle this problem, we apply non-maximal suppression to filter some feature corners based on a few criteria.\\
\\
We calculate the orientation of a keypoint by calculation of an Intensity Centroi​d. There is an analogy between Intensity Centroi​d and calculating center of mass in physics. The step of Intensity Centroi​d for sub-image S, is as follows,

\begin{enumerate}
	\item The torque in x-direction ($\tau_x$) is $\sum_{x,y\in S} x \cdot  I(x,y)$.
	\item The torque in y-direction ($\tau_y$) is $\sum_{x,y\in S} y \cdot  I(x,y)$.
	\item The total intensity (I) is $\sum_{x,y\in S} I(x,y)$.
    \item The center of intensity ($C_x$,$C_y$) is ($\frac{\tau_x}{I}, \frac{\tau_y}{I}$).
	\item The orientation of subset S is defined as arctan$(\frac{C_x}{C_y})$ = arctan($\frac{\tau_y}{\tau_x}$).
\end{enumerate}
Note that the steps above works are suitable for any 2d coordinate system.

\subsubsection{BRIEF descriptor}

\begin{figure}[htb]
  \centering
  \includegraphics[width=10cm]{Figure/ch2/selection_nbit_pair}
  \caption[Five different approaches to choosing the pairs of points]{Five different approaches to choosing the pairs of points\cite{Calonder10brief} }
  \label{fig:selection_nbit_pair}%
\end{figure}

\noindent
After extracting N "interesting" points, we need to describe those points. BRIEF is a binary feature descriptor\cite{Calonder10brief}, which is usually a 128 bit to 512 bit string. Since it is stored as a vector with elements only 0 or 1, it is easy to compute and do matching with other descriptors, which makes it extremely fast while having high repeatabilty at the same time.\\
\\
To construct a binary feature vector for an image patch, we first select n pairs of points based on different kinds of sampling method and perform binary test (t), so the BRIEF is vector of the responses of the binary test. Assume p(i,n) be the n-th element in the i-th pair, the i-th element of binary feature vector (v[i]) is calculated by the following equation,

\begin{equation}
v[i] = \begin{cases}
1 &\text{$p(i,0)>p(i,1)$}\\
0 &\text{$else$}
\end{cases}
\end{equation}

\subsection{Direct method}
Instead of constructing feature descriptor for each corner, the main idea of direct method is to use the intensity directly. Direct method has become a hot research topic due to several reasons.
\begin{enumerate}
	\item Compared to indirect method which construct thousands features, direct method take all pixel in the image into account, and thus make sure that no information is missing.
	\item Indirect method detects corners and creates descriptors. For a scene with no obvious texture, direct method can perform better.
\end{enumerate} 


\section{Features matching}
After creating features for each image in the sequence, we will find those features that also exist in other images. The idea of matching features is basically testing the similarity of two feature descriptors through their inner product, If it is smaller than the threshold, then it is considered as a match. However, it is expensive to perform bruce force matching to all features in those two images, hence we filter some candidates that do not match the geometric constraint. Here are two examples.

\subsection{Epipolar constraint}

\begin{figure}%
    \centering
    \subfloat[Epipolar constraint]{{\includegraphics[width=7cm]{Figure/ch2/epipolar_constraint} }}%
    \qquad
    \subfloat[Reprojection constraint]{{\includegraphics[width=7cm]{Figure/ch2/reprojection_constraint} }}%
    \caption{Visualization of geometric constraint}%
    \label{fig:geometric_constraint}%
\end{figure}

Given the keypoint position in one frame and the relative pose between two frames, we can draw an epipolar line on the second frame and the corresponding feature must lie on the line, see Fig \ref{fig:geometric_constraint}a\\
\\
Assume $x_1$ (in homogeneous coordinates) be the keypoint position in image coordinate of the first frame, X be the corresponding 3d position, R be the rotation from 1st camera coordinate to 2nd camera coordinate, -T is the displacement from 2nd camera center to 1st camera center. Therefore, we have,

\begin{equation}
\lambda_1 x_1 = X
\end{equation}

\begin{equation}
\lambda_2 x_2 = R X + T = \lambda_1 R x_1 + T
\end{equation}

\noindent
To remove addition by multiplying with $\hat{T}$ where $\hat{T} = 
\begin{pmatrix}
0   　&& -T_z &&  T_y\\
T_z 　&& 0    && -T_x\\
-T_y　&& T_x  && 0
\end{pmatrix}$,

\begin{equation}
\lambda_2 \hat{T} x_2 = \lambda_1 \hat{T} R x_1
\end{equation}

\noindent
Then we project the above equation to $x_2$, since $\lambda_1$ and $\lambda_2$ are not equal to 0,

\begin{equation}
x_2^T  \hat{T} R x_1 = 0
\label{eqn:epipolar_constraint_zero}
\end{equation}

\noindent
Owing to noise, pixel quantization and etc, it is very likely that there are no $x_1$ and $x_2$ satisfying eq \ref{eqn:epipolar_constraint_zero}. We would set a threshold $\epsilon$, which means that the set of $x_2$ does not need to lie on the epipolar line but just nearby it. Therefore, the epipolar constraint should be rewritten as,

\begin{equation}
|x_2^T  \hat{T} R x_1| < \epsilon
\end{equation}

\subsection{Reprojection constraint}
Given the 3d position of the feauture and the absolute pose of the second frame, we can shrink the search area into a small circle, see Fig \ref{fig:geometric_constraint}


\section{Probability theory in visual navigation}
There are many good methods to calculate close-form solution of poses given the 3D position of feature points and the corresponding points in the image, for example, PnP-ransac, 5-points and 8-points algorithm up to scale. However, every measurement contains noise, and there are problems like that
\begin{enumerate}
	\item the algorithm is noise sensitive
	\item no information about how accurate the solution is
	\item how to update the solution while there are more observations made
\end{enumerate}
Instead of calculating a close-form solution, which is the best solution the algorithm can give us, we calculate the probabilistic representation of the estimated solution, which is a probabilistic distribution over the space of all possible solution. With probabilistic representation, the uncertainty and degeneracy of the solution can be written in a mathematical way.

\subsection{Gaussian distribution}
The Gaussian distribution is widely used because
\begin{enumerate}
	\item it is governed by only two parameters (mean and variance) which can be easily obtained from data,
	\item the mathematics is simple and well defined
\end{enumerate}

\noindent The 1D Gaussian distribution and multivariate Gaussian are 
\begin{equation}
\mathcal{G}(x-\mu, \sigma_x^2) = \frac{1}{\sqrt{2\pi\sigma_x^2}}\exp(-\frac{(x-\mu)^2}{2\sigma_x^2}),
\end{equation}

\begin{equation}
\mathcal{G}(x-\mu, \Sigma) = \frac{1}{\sqrt{|2\pi\Sigma|}}\exp(-\frac{1}{2}(x-\mu)^T\Sigma^{-1}(x-\mu)).
\end{equation}


\subsection{Maximum likelihood (MLE) and Maximum a posteriori (MAP)}
If we want to find the probabilistic distribution of a "state variable" x, we can do some observation z. We can consider that z is a transformation of x that we can measure,
\begin{equation}
z = f(x) + \epsilon,
\end{equation}
where f is the response function and $\epsilon \sim \mathcal{G}(x-\mu, \sigma_x^2)$ is stochastic a variable used to describe noise.
\\\\
\textbf{Likelihood} is the probability distribution of measurement (z) given the true value f(x),
\begin{equation}
\mathcal{P}(z|x) = \int d\epsilon \; \mathcal{P}(z,\epsilon|x) = \int d\epsilon \; \mathcal{P}(z|\epsilon,x) \mathcal{P}(\epsilon|x)=\mathcal{G}(z-f(x), \sigma_x^2)
\label{eqn:MLE_def}
\end{equation}
\\
\textbf{The posterior} is the probability distribution of true value f(x) given the measurement (z). By using Bayes rule, we obtain the posterior by combining likelihood and prior of x as follows:
\begin{equation}
\mathcal{P}(x|z) = \frac{\mathcal{P}(z|x) \mathcal{P}(x) }{\int dy \; \mathcal{P}(z|y) \mathcal{P}(y)}.
\label{eqn:MAP_def}
\end{equation}
\\
There are many ways to define the "best guess" of a state (x) given measurement (z). One of the popular ways is to find $x^*$ such that it maximizes the a posterior, this is called Maximum a posteriori (MAP),
\begin{equation}
x^*_{MAP} = \underset{x}{\mathrm{argmax}} \; P(x|z) = \underset{x}{\mathrm{argmax}} \; P(z|x) P(x)
\end{equation}
Note that if we have a flat prior on x, the solution of MAP is equal to MLE.

\subsection{The Hamitonian (Energy) to minimize}
Let state $\textbf{x} = \lbrace x_1,x_2,...,x_N,y_1,y_2,...,y_M \rbrace$, where the state contains N camera poses and M mappoints, and $\textbf{u}=\lbrace u_1,u_2,...,u_{t=N} \rbrace$ is the control data which carry information about the changes of state, for example $u_t$ contains the change of state from time t-1 to time t, and $\textbf{z} = \lbrace z_{i,j}|i\in\lbrace1,2,...,N\rbrace, j\in\lbrace1,2,...,M\rbrace \rbrace$ contains all the measurements, $z_{i,j}$ can understand as the measurement of mappoint j with camera pose i. With all these variables, we can represent the probability distribution of a state as
\begin{equation}
x_t = f(x_{t-1},u_t) + \epsilon_f
\end{equation} 
\begin{equation}
z_{t,j} = h(x_t,y_j) + \epsilon_h
\end{equation} 
\begin{equation}
P(\textbf{\textit{x}}|\textbf{\textit{z}},\textbf{\textit{u}})
\end{equation} 
where $\epsilon_f \sim \mathcal{G}(0,F)$ and $\epsilon_h \sim \mathcal{G}(0,H)$, F and H are the covariances of the Gaussian distributions.\\
\\
The information E is defined as the negative $\ln$ to the posterior. The result is as follows,
\begin{equation}
r_i = x_i - f(x_{i-1}, u_i)
\end{equation} 
\begin{equation}
r_{i,j} = z_{i,j} - h(x_i, y_j)
\end{equation} 
\begin{equation}
E=\frac{1}{2} \sum_i r_i^T F_i^{-1} r_i + \frac{1}{2} \sum_{i,j} r_{i,j}^T H_{i,j}^{-1} r_{i,j}
\label{eqn:hamitonian_def}
\end{equation}
The first term is error term from measurement of speed and the second term is the reprojection error, flat prior has been used here.

\subsection{Optimization scheme}
\label{Optimization_scheme}

In order to find the MAP estimator $x^*$ by minimizing eq.\ref{eqn:hamitonian_def}, we can only solve it with numerical method. We update the state ($\delta x$) at each iteration until the Hamitonian converges to a local minimum (Since the residual is usually non-linear, and it is not guaranteed that the local minimum is the global minimum).

\subsubsection{Gradient descent}
Gradient descent is a first order iteration method. By taylor-expanding the response function $\parallel \textbf{f}(\textbf{x} + \delta \textbf{x}) \parallel^2$ up to first order,
\begin{equation}
\parallel \textbf{f}(\textbf{x} + \delta \textbf{x}) \parallel^2 \approx \parallel \textbf{f}(\textbf{x})\parallel^2 + \textbf{J}(\textbf{x})^T \delta \textbf{x}
\end{equation} 
\\
We can update the state along with the direction of negative gradient $-\textbf{J}(\textbf{x})$ with step size $\lambda$ according to
\begin{equation}
\textbf{x} \rightarrow  \textbf{x} - \textbf{J}(\textbf{x}) \lambda.
\end{equation} 

\subsubsection{Newton method}
When using Gradient descent, we need to determine the step size $\lambda$ at each iteration. In order to set the $\lambda$ automatically, we taylor-expand the response function f(x) around x up to second order,
\begin{equation}
\parallel \textbf{f}(\textbf{x} + \delta \textbf{x}) \parallel^2 \approx \parallel \textbf{f}(\textbf{x})\parallel^2 + \textbf{J}(\textbf{x})^T \delta \textbf{x} + \frac{1}{2} \; \delta \textbf{x}^T \textbf{H} \; \delta \textbf{x}.
\end{equation} 
We differentiate the above equation with respect to $\delta\textbf{x}$, let LHS to be zero and rearrange the $\delta\textbf{x}$ as subject, to obtain
\begin{equation}
\textbf{x} \rightarrow  \textbf{x} - \textbf{H}^{-1} \textbf{J}(\textbf{x})
\end{equation} 

\subsubsection{Gaussian Newton method}
Calculating second order derivative is expensive. Instead of expanding $\parallel \textbf{f}(\textbf{x} + \delta \textbf{x}) \parallel^2$, we expand $\textbf{f}(\textbf{x} + \delta \textbf{x})$
\begin{equation}
\parallel \textbf{f}(\textbf{x} + \delta \textbf{x}) \parallel^2  \approx \parallel \textbf{f}(\textbf{x}) + \textbf{J}(\textbf{x}) \delta \textbf{x} \parallel^2 = \parallel \textbf{f}(\textbf{x})\parallel^2 + 2 \textbf{f}(\textbf{x})^T \textbf{J}(\textbf{x}) \delta \textbf{x}    + \delta \textbf{x}^T \textbf{J}(\textbf{x})^T \textbf{J}(\textbf{x}) \delta \textbf{x} 
\end{equation} 
We differentiate above equation with respect to $\delta\textbf{x}$, let LHS to be zero and rearrange the $\delta\textbf{x}$ as subject, we obtain
\begin{equation}
\textbf{x} \rightarrow  \textbf{x} - (\textbf{J}(\textbf{x})^T \textbf{J}(\textbf{x}))^{-1} \textbf{J}(\textbf{x})^T \textbf{f}(\textbf{x})
\end{equation} 


\subsubsection{Levenberg-Marquardt method}
Levenberg-Marquardt method is a hybrid method of Gradient descent and Gaussian Newton method. The idea of this method is that if Gaussian Newton method cannot optimize the energy in the right way, it would slightly turn the model into Gradient descent by increasing $\lambda$
\begin{equation}
\textbf{x} \rightarrow  \textbf{x} - (\textbf{J}(\textbf{x})^T \textbf{J}(\textbf{x}) + \lambda \mathrm{diag}(\textbf{J}(\textbf{x})^T \textbf{J}(\textbf{x})))^{-1} \textbf{J}(\textbf{x})^T \textbf{f}(\textbf{x})
\end{equation} 


\newpage
\section{Keypoints triangulation}
\begin{figure}[htb]
  \centering
  \includegraphics[width=10cm]{Figure/ch2/triangulation}
  \caption{Obtain depth through triangulation}
  \label{fig:triangulation}%
\end{figure}

\noindent
Keypoints on images are represented by 2d-homogeneous coordinate. To obtain the depth information of keypoints, we find correspondences between images by indirect feature matching or optical flow and etc.\\
\\
Given two corresponding points $\textbf{x}_1$, $\textbf{x}_2$ on two different images and together with the relative pose, we can triangulate keypoints as follows. Let $\textbf{p}_{c1}=s_1 \textbf{x}_1$ and $\textbf{p}_{c2} = s_2 \textbf{x}_2$ to be the 3d position of the same mappoint with different camera coordinates, where $s_1$ and $s_2$ are the scale for homogeneous coordinate. So that $\textbf{x}_1$ and $\textbf{x}_2$ are related by the following equation,
\begin{equation}
s_1 \textbf{x}_1 = p_{c1} = R (s_2 \textbf{x}_2) + \textbf{t}
\end{equation} 
Multiply both sides with $\textbf{x}_1^\wedge$ ($a^\wedge b = a \times b$) to find $s_2$,
\begin{equation}
 s_2  =-\frac{|\textbf{x}_1^\wedge\textbf{t}|}{|\textbf{x}_1^\wedge R  \textbf{x}_2|} 
\end{equation} 
Note that due to the noise, the preimages of the correspondence may not intersect, and this method no longer works. We have to solve it by least square method (used in our work).


